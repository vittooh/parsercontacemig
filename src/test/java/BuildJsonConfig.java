import com.hugo.vitor.ocr.config.Configuracao;
import com.hugo.vitor.ocr.singleton.GsonSingleton;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

/**
 * criado por vitor em 05/02/19
 **/
public class BuildJsonConfig {

    @Test
    public void build() throws IOException {
        Configuracao c = new Configuracao("/tmp/");

        Files.write(
                Paths.get("config.json"),
                Collections.singletonList(
                        GsonSingleton.getInstance().toJson(c)
                )
        );

    }
}
