package com.hugo.vitor.ocr.config;

import java.io.Serializable;

/**
 * criado por vitor em 05/02/19
 **/
public class Configuracao  implements Serializable {
    private static final long serialVersionUID = -129255558482510400L;

    private String pathArquivos;

    public Configuracao() {
    }

    public Configuracao(String pathArquivos) {
        this.pathArquivos = pathArquivos;
    }

    public String getPathArquivos() {
        return pathArquivos;
    }

    public void setPathArquivos(String pathArquivos) {
        this.pathArquivos = pathArquivos;
    }
}
