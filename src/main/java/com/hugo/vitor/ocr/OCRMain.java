package com.hugo.vitor.ocr;

import com.hugo.vitor.ocr.parser.PdfParser;
import com.hugo.vitor.ocr.singleton.ConfigSingleton;
import com.hugo.vitor.ocr.singleton.GsonSingleton;

import java.io.File;
import java.io.FileNotFoundException;

public class OCRMain {

    public static void main(String[] args) {
        initSingletons(new File(args[0]));
        System.out.println(new PdfParser(ConfigSingleton.getInstance()).call());
    }

    private static void initSingletons(File f) {
        GsonSingleton.getInstance();
        try {
            ConfigSingleton.initialize(f);
        } catch (FileNotFoundException e) {
            System.out.println("Algo de errado ocorreu ao importar configs !!!");
        }
    }
}
