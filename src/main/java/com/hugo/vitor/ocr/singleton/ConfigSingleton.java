package com.hugo.vitor.ocr.singleton;

import com.hugo.vitor.ocr.config.Configuracao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * criado por vitor em 05/02/19
 **/
public class ConfigSingleton {

    private static Configuracao configuracao;

    public static void initialize(File file) throws FileNotFoundException {
        configuracao = GsonSingleton
                .getInstance()
                .fromJson(
                        new FileReader(file),
                        Configuracao.class
                );
    }

    public  static Configuracao getInstance() {
        return configuracao;
    }
}
