package com.hugo.vitor.ocr.singleton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * criado por vitor em 05/02/19
 **/
public class GsonSingleton {

    private static  Gson gson;

    public static Gson getInstance(){
        if(gson == null){
            synchronized (GsonSingleton.class){
                gson = new GsonBuilder()
                        .disableHtmlEscaping()
                        .setPrettyPrinting()
                        .create();
            }
        }

        return  gson;
    }
}
