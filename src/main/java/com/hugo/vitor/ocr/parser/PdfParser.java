package com.hugo.vitor.ocr.parser;

import com.hugo.vitor.ocr.config.Configuracao;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.concurrent.Callable;

/**
 * criado por vitor em 05/02/19
 **/
public class PdfParser implements Callable<String> {


    private Configuracao configuracao;

    public PdfParser(Configuracao configuracao) {
        this.configuracao = configuracao;
    }

    public String call() {
        File diretorio = new File(configuracao.getPathArquivos());

        File[] files = diretorio.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".pdf");
            }
        });

        if (files == null) {
            System.out.println("Nothing to do!!");
            return null;
        }

        for (File f : files) {
            try {
                PDDocument pdf = PDDocument.load(f);
                return new PDFTextStripper().getText(pdf);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
